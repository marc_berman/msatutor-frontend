var msaApp = angular.module('msaApp', ['ui.router', 'LocalStorageModule', 'ngAnimate', 'ngTouch', 'ngSanitize', 'ui-notification', '720kb.datepicker', 'ui.bootstrap']);

msaApp.config(['$httpProvider', '$stateProvider', '$urlRouterProvider', function ($httpProvider, $stateProvider, $urlRouterProvider) {
    // $locationProvider.html5Mode(true);
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/");
    //
    // Now set up the states
    $stateProvider
        .state('home', {
            url: "/",
            templateUrl: "views/home.html",
            controller: "HomeCtrl"
        })

        .state('login', {
            url: "/login",
            templateUrl: "views/login.html",
            controller: "LoginCtrl"
        })

        .state('register', {
            url: "/register",
            templateUrl: "views/register.html",
            controller: "RegisterCtrl"
        })

        .state('booking', {
            url: "/booking",
            templateUrl: "views/booking.html",
            controller: "BookingCtrl"
        })
        .state('faq', {
            url: "/faq",
            templateUrl: "views/faq.html",
            controller: "FaqCtrl",
        })
        .state('adminreport', {
            url: "/admin/report",
            templateUrl: "views/admin/adminreport.html",
            controller: "ReportCtrl",
            params: {
                restricted: true
            }
        })
        .state('adminviewtransaction', {
            url: "/admin/transactions",
            templateUrl: "views/admin/adminviewtransaction.html",
            controller: "TransactionAdminCtrl",
            params: {
                restricted: true
            }
        })
        .state('tutortransaction', {
            url: "/tutor/transactions",
            templateUrl: "views/tutor/transaction.html",
            controller: "TransactionTutorCtrl",
            params: {
                restricted: true
            }
        })
        .state('tutortimesheet', {
            url: "/tutor/timesheet",
            templateUrl: "views/tutor/tutortimesheet.html",
            controller: "TimesheetCtrl",
            params: {
                restricted: true
            }
        })
    ;
}]);

msaApp.run(['$rootScope', '$state', '$location', 'Notification', function ($rootScope, $state, Notification) {
    $rootScope.$on("$stateChangeStart", function (event, nextRoute, currentRoute) {
        var aac;
        if (aac = nextRoute && nextRoute.params && nextRoute.params.restricted) {
            // if (!$localStorage.User) {
            //     event.preventDefault();
            //     Notification.error("You are trying to access a restricted area, please login and try again");
            //     $state.go("login");
            // }
        }
    });
}]);