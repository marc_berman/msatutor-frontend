msaApp.controller("NavCtrl", ['$scope', '$location',
    function ($scope, $location) {
        $scope.toggleMenu = function () {
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        };
    }
]);

msaApp.controller("HomeCtrl", ['$scope', '$location',
    function ($scope, $location) {

    }
]);

msaApp.controller("LoginCtrl", ['$scope', '$location',
    function ($scope, $location) {

    }
]);

msaApp.controller("RegisterCtrl", ['$scope', '$location',
    function ($scope, $location) {

    }
]);

msaApp.controller("FaqCtrl", ['$scope', '$location',
    function ($scope, $location) {

    }
]);

msaApp.controller("BookingCtrl", ['$scope', '$location',
    function ($scope, $location) {

    }
]);

msaApp.controller("ReportCtrl", ['$scope', '$location',
    function ($scope, $location) {

    }
]);

msaApp.controller("TransactionAdminCtrl", ['$scope', '$location',
    function ($scope, $location) {

    }
]);

msaApp.controller("TransactionTutorCtrl", ['$scope', '$location',
    function ($scope, $location) {

    }
]);

msaApp.controller("TimesheetCtrl", ['$scope', '$location',
    function ($scope, $location) {

    }
]);