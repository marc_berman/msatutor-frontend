msaApp.factory('DataService', function ($http, RestApiUri) {
    var _datafactory = {};

    var requestApi = {
        http: function (methode, location, data) {
            if (methode === "GET") {
                return $http({
                    method: methode,
                    url: location
                });
            }
            if (methode === "POST") {
                return $http({
                    method: methode,
                    url: location,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
            }
        }
    };

    _datafactory.bookingList = function () {
        return requestApi.http('GET', RestApiUri.UserLoginUri);
    };

    _datafactory.bookingCreate = function (data) {
        return requestApi.http('POST', RestApiUri.BookingCreateUri, data);
    };

    _datafactory.loginUser = function (data) {
        return requestApi.http('POST', RestApiUri.UserLoginUri, data);
    };

    _datafactory.updateUser = function (data) {
        return requestApi.http('POST', RestApiUri.UserUpdateUri, data);
    };

    _datafactory.createUser = function (data) {
        return requestApi.http('POST', RestApiUri.UserCreateUri, data);
    };

    _datafactory.listUsers = function () {
        return requestApi.http('GET', RestApiUri.UserListUri);
    };

    _datafactory.listByUsername = function (username) {
        return requestApi.http('GET', RestApiUri.UserUsernameUri + username);
    };

    _datafactory.listTutors = function () {
        return requestApi.http('GET', RestApiUri.UserTutorUri);
    };

    _datafactory.listAdmins = function () {
        return requestApi.http('GET', RestApiUri.UserAdminUri);
    };

    _datafactory.addSurvey = function (data) {
        return requestApi.http('POST', RestApiUri.SurveyAddUri, data);
    };

    _datafactory.listSurvey = function () {
        return requestApi.http('GET', RestApiUri.SurveyListUri);
    };

    _datafactory.sumSurvey = function () {
        return requestApi.http('GET', RestApiUri.SurveySumUri);
    };

    return _userfactory;
});

msaApp.factory('RestApiUri', function () {
    var domain = "http://138.68.137.29:59898/api/v1/";
    // var domain = "http://localhost:57569/umbraco/api/";

    var apiUri = {

        //User URI
        BookingListUri: domain + "booking/list/",
        BookingCreateUri: domain + "booking/create/",
        UserLoginUri: domain + "user/login/",
        UserUpdateUri: domain + "user/update/",
        UserCreateUri: domain + "user/create/",
        UserListUri: domain + "user/list/",
        UserUsernameUri: domain + "user/byusername/",
        UserTutorUri: domain + "user/tutors/",
        UserAdminUri: domain + "user/admins/",
        SurveyAddUri: domain + "survey/add/",
        SurveyListUri: domain + "survey/list/",
        SurveySumUri: domain + "survey/sumtypes/"


    };

    return apiUri;
});